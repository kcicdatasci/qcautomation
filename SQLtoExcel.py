__author__ = 'barnettr'
import pandas.io.sql as pdSQL
import pandas as pd
import pyodbc

def Read(sql, conn):
    df = pdSQL.read_sql(sql, conn)
    return df

def dfToExcel(df, xlsxFile):
    return df.to_excel(xlsxFile, index=False)


def Query(sql, conn):
    QueryResults = pdSQL.read_sql(sql, conn)
    queryColumnCount = QueryResults.shape[1]
    queryRowCount = QueryResults.shape[0]
    print("cols: " + str(queryColumnCount)) # can be removed
    print("rows: " + str(queryRowCount)) # can be removed
    return QueryResults


def QueryToQCdf(query_a, query_b):
    query_a_columns = query_a.shape[1]
    query_a_rows = query_a.shape[0]
    query_b_columns = query_b.shape[1]
    query_b_rows = query_b.shape[0]
    query_headers = query_a.columns.values.tolist()
    if query_a_columns == query_b_columns and query_a_rows == query_b_rows:
        query_col_idx_len = query_a_columns
    else:
        return print("Query column or row count does not match.")

    idx_padding = 3 # can be used with query_col_idx_len to create styling padding for excel exports
    consistency_df = pd.DataFrame(index=range(0,query_col_idx_len), columns=query_headers)

    def PopulateConsistencydf():
        #### TODO: create a function that inserts an equation formula against the two query dfs, populating the consistency df with excel formula "IF(A1=A4, 0,1)"; take current cell and subtract len of columns for comparison value a and take the current cell and add the length of columns to get value b comparison, store these as variables and insert as a string concat formula before writing to excel
        ### TODO: or the smarter way would be to use the built in pandas.equals?
        return 0
    frames = [query_a, consistency_df, query_b]
    qc_table_concat = pd.concat(frames, axis=1)
    return qc_table_concat

#### Set variables

# Only change server and db
server = 'HULK'
db = 'Demo'
conn = pyodbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + db + ';Trusted_Connection=yes')

## Store raw SQL queries as strings

# Sample query A
sql_select_tblStatus = """
SELECT [StatusID]
      ,[Status]
  FROM [Demo].[dbo].[tblStatus]

    """

# Sample query B
sql_select_tblStatusAlt = """
SELECT [StatusID]
      ,[Status]
  FROM [Demo].[dbo].[tblStatus]

    """

# Run python function to query the SQL string and store the pandas dataframe as a variable
qc_query = Query(sql_select_tblStatus, conn)
data_query = Query(sql_select_tblStatusAlt, conn)

# Combine two query dataframes to one data frame, separating the two with a consistency section of the same index width
qc_df = QueryToQCdf(qc_query, data_query)
print(qc_df)

# name the xlsx file to be output
qc_template_xlsx = 'qc_template_output.xlsx'

# export the QC dataframe to an excel file
dfToExcel(qc_df, qc_template_xlsx)

















#### Unused code; can be cut or kept as reference
# def Execute(conn, sql, *parameterValues):
#     ##not sure if the sproc exec is working yet as the sproc code is not running in MSSQL either
#     df = pdSQL.execute(sql, conn, parameterValues)
#     return df
#

# def WriteToCSV(csvFile, df):
#     return df.to_csv(csvFile, index=False)
#
#
# def AppendToCSV(csvFile, df):
#     return df.to_csv(csvFile, header=False, mode='a', index=False)
#
#
# def CSVToExcel(csvFile, xlsxFile):
#     df = pd.read_csv(csvFile)
#     return df.to_excel(xlsxFile, index=False)
# def QueryWritetoCSV(query, csv):
#     # Ideally, we would just select from a view that contains this query.
#
#     return WriteToCSV(csv, query)

# csv = 'output.csv'
# qc_query_csv = QueryWritetoCSV(qc_query, csv)


